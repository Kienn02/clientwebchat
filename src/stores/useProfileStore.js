import { defineStore } from 'pinia'


export const useProfileStore  = defineStore('Profile', {
    
  state: () => ({
    isShow : false
  }),
  
  actions: {
    showHandle(){
        this.isShow = !this.isShow
    }
  }
})
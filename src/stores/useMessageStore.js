import { defineStore } from 'pinia'

export const useMessageStore = defineStore('message', {
    state: () => ({
        messages: [],
        groupName: null,
    }),

    actions: {
        async getMessages(groupId) {
            await axios
                .get("http://127.0.0.1:8000/api/groups/" + groupId, {
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + localStorage.getItem('token')
                    },
                }).then((resp) => {
                    // console.log(resp.data.group.messages);
                    this.messages = resp.data.group.messages
                    // console.log(this.messages);
                })
        },

        setNameGroup(groupName){
            this.groupName = groupName
        },

        setNewChat(chat){
            this.messages.push(chat)
        },

        async addMessage(groupId, content, attachment){
            await axios
                .post("http://127.0.0.1:8000/api/messages/",{
                    'group_id' : groupId,
                    'content' : content,
                    'attachment' : attachment
                }, {
                    headers: {
                        'Content-Type' : 'multipart/form-data',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + localStorage.getItem('token')
                    },
                }).then((resp) => {
                    // console.log(resp.data);
                    this.messages.push(resp.data.message)
                })

        },

        saveGroupName(newGroupName){
            this.groupName = newGroupName
        }
    }
})
import { defineStore } from 'pinia'


export const useFormStore  = defineStore('Form', {
    
  state: () => ({
    isShowEditGroupName: false,
    isShowAddMember : false,
    stackMembers: [],
    stackUserId: []
  }),
  
  actions: {
    showEditGroupNameHandler(){
        this.isShowEditGroupName = !this.isShowEditGroupName
    },
    showAddMemberHandler(){
      this.isShowAddMember = !this.isShowAddMember
    },

    addStackMemberHandler(member){
      const check = this.stackMembers.findIndex(user => user.id === member.id)
      console.log(check);
      if(check == -1){
        this.stackUserId.push(member.id)
        console.log(this.stackUserId);
        return this.stackMembers.push(member)
      }
      this.stackUserId = this.stackUserId.filter(id => id !== member.id)
      console.log(this.stackUserId);
      
      return this.stackMembers = this.stackMembers.filter(user => user.id !== member.id)
    },

    removeStackHandler(){
      this.stackMembers = []
      this.stackUserId = []
    },

    removeMemberFromStack(userId){
      this.stackUserId = this.stackUserId.filter(id => id !== userId)
      this.stackMembers = this.stackMembers.filter(user => user.id !== userId)
    },

    async addMemberHandler(groupId){
      await axios
        .post("http://127.0.0.1:8000/api/groups/users" , {
          'user_id' : this.stackUserId,
          'group_id' : groupId
        },{
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
          },
        }).then((resp) => {
          console.log(resp);
        })
    },
  }
})
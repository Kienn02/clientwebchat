import { defineStore } from 'pinia'

export const useGroupStore = defineStore('group', {
    state: () => ({
        groups: [],
        idGroup: null,
        memberGroup: [],
        avatarGroup: null,
        users: []
    }),

    actions: {
        async getGroups() {
            await axios
                .get("http://127.0.0.1:8000/api/groups", {
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + localStorage.getItem('token')
                    },
                }).then((resp) => {
                    this.groups = resp.data.groups
                })
        },
        
        setMemberGroup(index){
            // this.getGroups()
            this.memberGroup = this.groups[index].users
        },

        setAvatarGroup(avatarGroup){
            this.avatarGroup = avatarGroup
        },
        
        async searchUser(value){
            await axios
                .get("http://127.0.0.1:8000/api/users", {
                    params:{
                        phone: value
                    },
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + localStorage.getItem('token')
                    },
                }).then((resp) => {
                    this.users = resp.data.users
                    // console.log(this.users);
                })
        },

        async createGroup(userId){
            await axios
                .post("http://127.0.0.1:8000/api/groups", {
                        user_ids: [userId]
                    },{
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + localStorage.getItem('token')
                    }
                }).then((resp) => {

                    this.idGroup = resp.data.group
                })
        },
        async updateGroupHandler(newGroupName, newAvatar, groupId){
            await axios
                .put("http://127.0.0.1:8000/api/groups/" + groupId , {
                        'name': newGroupName,
                        'avatar': newAvatar
                    }, {
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + localStorage.getItem('token')
                    }
                }).then((resp) => {
                    console.log(resp);
                    // this.idGroup = resp.data.group  multipart/form-data
                })
        }
    }
})
import { defineStore } from 'pinia'

export const useAuthStore  = defineStore('auth', {
    
  state: () => ({
    current_user: {},
    check: false
  }),
  
  actions: {
    
    async login( phoneNumber, passWord){
      await axios
        .post("http://127.0.0.1:8000/api/auth/login",{
            phonenumber : phoneNumber,
            password : passWord
        })
        .then(( resp ) => {
            if(resp.data.access_token){
                localStorage.setItem('token', resp.data.access_token) 
                this.getProfile()
            }
            // else{
            // localStorage.setItem('token', '') 
            //     alert('Sai Thông Tin Tài Khoản');
            // }
        })
        .catch( error => {
            alert('Sai Thông Tin Tài Khoản');
            localStorage.setItem('token', '') 
        })
    },
    async getProfile(){
      await axios
        .get("http://127.0.0.1:8000/api/auth/profile",{
            headers : {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            }
        })
        .then(( resp ) => {
            localStorage.setItem('id', resp.data.user.id) 
            localStorage.setItem('name', resp.data.user.name) 
            localStorage.setItem('phoneNumber', resp.data.user.phoneumber) 
            localStorage.setItem('avatar', resp.data.user.avatar_user) 
        })
    },

    register( name, phoneNumber, passWord, avatar) {
      axios
        .post("http://127.0.0.1:8000/api/auth/register",{
            name : name,
            phonenumber : phoneNumber,
            password : passWord,
            avatar : avatar
        },{
            headers: {
                'Content-Type' : 'multipart/form-data'
            }
        })
        .then( resp => {
            console.log( resp )
            alert('Đăng ký thành công');
        })
        .catch( error => {
            console.log( error )
            alert('Đăng ký thất bại');
        })
    }
  }
})
import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'login',
    component: () => import('../views/AccountView.vue'),
    // beforeEnter: (el, done, to, from, next) => {
    //   if (localStorage.getItem('token') == '') {
    //     next()    
    //   }
    // }
  },
  {
    path: '/message/:id?',
    name: 'message',
    
    component: () => import('../views/MessageView.vue'),
    beforeEnter: (to, from, next) => {
      if (localStorage.getItem('token') == ''){
        next('/')
      }
      next()
    }
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  scrollBehavior(to, from, savedPosition) {
    return{
      el: '#app_right-body',
      top: 100,
    }
  },  
})

export default router

import { createApp } from 'vue'
import router from './router'
import { createPinia } from 'pinia'
import LaravelEcho from "laravel-echo-vue3"

import { 
    DatePicker, 
    message 
} from 'ant-design-vue'
import App from './App.vue'
import axios from 'axios'
window.axios = axios

import 'ant-design-vue/dist/reset.css';
import "./assets/css/reset.css"
import "./assets/css/base.css"

const app = createApp(App)
app.use(router)
app.use(DatePicker)
app.use(createPinia())

const options = {
    broadcaster: 'socket.io',
    host: window.location.hostname + ':6001',
    auth: {
        headers: {
            Authorization : 'Bearer ' + localStorage.getItem('token')
        }
    }
}

app.use(LaravelEcho, {
    ...options,
})

app.mount('#app')
app.config.globalProperties.$message = message


